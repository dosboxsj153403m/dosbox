// DOSBox, Scrum.org, Professional Scrum Developer Training
// Authors: Rainer Grau, Daniel Tobler, Z�hlke Technology Group
// Copyright (c) 2012 All Right Reserved

using System.Collections.Generic;
using System.Data;
using System.Linq;
using DosBox.Command.Framework;
using DosBox.Filesystem;
using DosBox.Interfaces;

namespace DosBox.Command.Library
{
    /// <summary>
    /// Command to change current directory.
    /// Example for a command with optional parameters.
    /// </summary>
    public class CmdRm : DosCommand
    {
        private static readonly string SYSTEM_CANNOT_FIND_THE_FILE_OR_DIRECTORY_SPECIFIED = "The system cannot find the file or directory specified.";

        public CmdRm(string name, IDrive drive)
            : base(name, drive)
        {
        }

        protected override bool CheckNumberOfParameters(int numberOfParametersEntered)
        {
            return numberOfParametersEntered == 1;
        }

        protected override bool CheckParameterValues(IOutputter outputter)
        {
            FileSystemItem file = Drive.GetItemFromPath(GetParameterAt(0));
            if (file != null)
            {
                return true;
            }
            outputter.PrintLine(SYSTEM_CANNOT_FIND_THE_FILE_OR_DIRECTORY_SPECIFIED);
            return false;
        }

        public override void Execute(IOutputter outputter)
        {
            FileSystemItem file = Drive.GetItemFromPath(GetParameterAt(0));
            Drive.CurrentDirectory.Remove(file);
        }



    }
}