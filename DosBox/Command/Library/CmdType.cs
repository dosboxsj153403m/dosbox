// DOSBox, Scrum.org, Professional Scrum Developer Training
// Authors: Rainer Grau, Daniel Tobler, Z�hlke Technology Group
// Copyright (c) 2012 All Right Reserved

using System.Collections.Generic;
using System.Data;
using System.Linq;
using DosBox.Command.Framework;
using DosBox.Filesystem;
using DosBox.Interfaces;

namespace DosBox.Command.Library
{
    /// <summary>
    /// Command to change current directory.
    /// Example for a command with optional parameters.
    /// </summary>
    public class CmdType : DosCommand
    {
        private static readonly string SYSTEM_CANNOT_FIND_THE_FILE_SPECIFIED = "The system cannot find the file specified.";

        public CmdType(string name, IDrive drive)
            : base(name, drive)
        {
        }

        protected override bool CheckNumberOfParameters(int numberOfParametersEntered)
        {
            return numberOfParametersEntered == 1;
        }

        protected override bool CheckParameterValues(IOutputter outputter)
        {
            FileSystemItem file = Drive.GetItemFromPath(GetParameterAt(0));
            if (file != null && !file.IsDirectory())
            {
                return true;
            }
            outputter.PrintLine(SYSTEM_CANNOT_FIND_THE_FILE_SPECIFIED);
            return false;
        }

        public override void Execute(IOutputter outputter)
        {
            File file = Drive.GetItemFromPath(GetParameterAt(0)) as File;
            if(file != null)
                outputter.Print(file.FileContent);
   
        }



    }
}