// DOSBox, Scrum.org, Professional Scrum Developer Training
// Authors: Rainer Grau, Daniel Tobler, Z�hlke Technology Group
// Copyright (c) 2012 All Right Reserved

using DosBox.Command.Framework;
using DosBox.Filesystem;
using DosBox.Interfaces;

namespace DosBox.Command.Library
{
    public class CmdMkFile : DosCommand
    {
        private static readonly string PARAMETER_CONTAINS_BACKLASH = "At least one parameter denotes a path rather than a directory name.";
        
        public CmdMkFile(string cmdName, IDrive drive)
            : base(cmdName, drive)
        {
        }

        protected override bool CheckNumberOfParameters(int numberOfParameters)
        {
            if (GetParameterAt(0).Length == 0)
                return false;
            return true;
        }

        protected override bool CheckParameterValues(IOutputter outputter)
        {

            if (ParameterContainsBacklashes(GetParameterAt(0), outputter))
                return false;
            else
                return true;
        }

        public override void Execute(IOutputter outputter)
        {
            string fileName = GetParameterAt(0);
            string fileContent = GetParameterAt(1);
            if (fileName.Length >= 0)
            {
                File newFile = new File(fileName, fileContent);
                this.Drive.CurrentDirectory.Add(newFile);
            }
        }

        private static bool ParameterContainsBacklashes(string parameter, IOutputter outputter)
        {
            if (parameter.Contains("\\") || parameter.Contains("/"))
            {
                outputter.PrintLine(PARAMETER_CONTAINS_BACKLASH);
                return true;
            }
            return false;
        }
    }
}