﻿// DOSBox, Scrum.org, Professional Scrum Developer Training
// Authors: Rainer Grau, Daniel Tobler, Zühlke Technology Group
// Copyright (c) 2012 All Right Reserved

using DosBox.Command.Library;
using DosBox.Filesystem;
using DosBoxTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DosBoxTest.Command.Library
{
    [TestClass]
    public class ExtendedCmdMkDirTestWithGherkin : CmdTest
    {

        [TestInitialize]
        public void setUp()
        {
            // Add all commands which are necessary to execute this unit test
            // Important: Other commands are not available unless added here.
            this.commandInvoker.AddCommand(new CmdMkDir("mkdir", this.drive));
        }

        [TestMethod]
        public void CmdMkDir_CreateNewDirectory_NewDirectoryIsAdded()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("mkdir test1");
            ThenDirectoryIsCreated("C:\\test1");
        }

        [TestMethod]
        public void CmdMkDir_CreateNewDirectory_NewDirectoryIsAddedToCorrectLocation()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIs(this.subDir1);
            WhenExecutingCommand("mkdir test1");
            ThenDirectoryIsCreated("C:\\subDir1\\test1");
        }

        [TestMethod]
        public void CmdMkDir_SingleLetterDirectory_NewDirectoryIsAdded()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("mkdir a");
            ThenDirectoryIsCreated("C:\\a");
            ThenNoErrorMessageIsPrinted();
        }

        [TestMethod]
        public void CmdMkDir_NoParameters_ErrorMessagePrinted()
        {
            GivenEmptyDirectoryStructure();
            WhenExecutingCommand("mkdir");
            ThenNoDirectoryIsCreated();
            ThenOutputContains("syntax of the command is incorrect");
        }

        [TestMethod]
        public void CmdMkDir_ParameterContainsBacklash_ErrorMessagePrinted()
        {
            GivenEmptyDirectoryStructure();
            WhenExecutingCommand("mkdir c:\\test1");
            ThenOutputContains("At least one parameter");
            ThenOutputContains("path rather than a directory name");
        }

        [TestMethod]
        public void CmdMkDir_ParameterContainsBacklash_NoDirectoryCreated()
        {
            GivenEmptyDirectoryStructure();
            WhenExecutingCommand("mkdir c:\\test1");
            ThenNoDirectoryIsCreated();
        }

        [TestMethod]
        public void CmdMkDir_SeveralParameters_SeveralNewDirectoriesCreated()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("mkdir test1 test2 test3");
            ThenDirectoryIsCreated("c:\\test1");
            ThenDirectoryIsCreated("c:\\test2");
            ThenDirectoryIsCreated("c:\\test3");
            ThenNumberOfDirectoriesCreatedIs(3);
        }

        [TestMethod]
        public void CmdMkDir_AllParametersAreReset()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("mkdir test1");
            WhenExecutingCommand("mkdir");  // Bad programming may lead to storing test1 as parameter and creating a new test1 directory with the empty mkdir command.
            ThenNumberOfDirectoriesCreatedIs(1);
            ThenOutputContains("The syntax of the command is incorrect");
        }
    }
}
