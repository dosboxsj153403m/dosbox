// DOSBox, Scrum.org, Professional Scrum Developer Training
// Authors: Rainer Grau, Daniel Tobler, Z�hlke Technology Group
// Copyright (c) 2012 All Right Reserved

using DosBox.Filesystem;
using DosBox.Interfaces;
using DosBox.Invoker;
using DosBoxTest.Command.Framework;
using DosBoxTest.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DosBoxTest.Command.Library
{
    public abstract class CmdTest
    {
        protected CommandInvoker commandInvoker = new CommandInvoker();
        protected IDrive drive = new Drive("C");
        protected TestOutput testOutput = new TestOutput();
        protected Directory rootDir;
        protected File file1InDir1;
        protected File file2InDir1;
        protected File fileInRoot1;
        protected File fileInRoot2;
        protected int numbersOfDirectoriesBeforeTest;
        protected int numbersOfFilesBeforeTest;
        protected Directory subDir1;
        protected Directory subDir2;

        public CmdTest()
        {
            this.rootDir = this.drive.RootDirectory;
        }

        public void CreateTestFileStructure()
        {
            GivenStandardDirectoryStructure();
        }

        protected void ExecuteCommand(string commandLine)
        {
            if (this.commandInvoker == null)
                this.commandInvoker = new CommandInvoker();
            this.commandInvoker.ExecuteCommand(commandLine, testOutput);
        }

        protected void GivenEmptyDirectoryStructure()
        {
            // Nothing to do!
        }

        /**Sets up a directory structure as follows:
         * C:\
         * |---FileInRoot1 ("an entry")
         * |---FileInRoot2 ("a long entry in a file")
         * |---subDir1
         * |   |---File1InDir1 (empty)
         * |   |---File2InDir1 (empty)
         * |---subdir2
         */
        protected void GivenStandardDirectoryStructure()
        {
            this.fileInRoot1 = new File("FileInRoot1", "an entry");
            this.rootDir.Add(this.fileInRoot1);
            this.fileInRoot2 = new File("FileInRoot2", "a long entry in a file");
            this.rootDir.Add(this.fileInRoot2);

            this.subDir1 = new Directory("subDir1");
            this.rootDir.Add(subDir1);
            this.file1InDir1 = new File("File1InDir1", "");
            this.subDir1.Add(this.file1InDir1);
            this.file2InDir1 = new File("File2InDir1", "");
            this.subDir1.Add(this.file2InDir1);

            this.subDir2 = new Directory("subDir2");
            this.rootDir.Add(subDir2);

            this.numbersOfDirectoriesBeforeTest = this.drive.RootDirectory.GetNumberOfContainedDirectories();
            this.numbersOfFilesBeforeTest = this.drive.RootDirectory.GetNumberOfContainedFiles();
        }

        protected void AndCurrentDirectoryIsRoot()
        {
            this.drive.ChangeCurrentDirectory(rootDir);
        }

        protected void AndCurrentDirectoryIs(Directory newCurrentDirectory)
        {
            this.drive.ChangeCurrentDirectory(newCurrentDirectory);
        }

        protected void WhenExecutingCommand(string commandLine)
        {
            if (this.commandInvoker == null)
                this.commandInvoker = new CommandInvoker();
            this.commandInvoker.ExecuteCommand(commandLine, this.testOutput);
        }

        protected void ThenDirectoryIsCreated(string createdDirectory)
        {
            Assert.IsTrue(createdDirectory.ToLower().StartsWith("c:\\"));
            FileSystemItem fsi = drive.GetItemFromPath(createdDirectory);
            Assert.IsNotNull(fsi);
            Assert.IsTrue(fsi.IsDirectory());
        }

        protected void ThenNoErrorMessageIsPrinted()
        {
            TestHelper.AssertOutputIsEmpty(testOutput);
        }

        protected void ThenOutputContains(string expectedOutput)
        {
            TestHelper.AssertContains(expectedOutput, this.testOutput);
        }

        protected void ThenNoDirectoryIsCreated()
        {
            ThenNumberOfDirectoriesCreatedIs(0);
        }

        protected void ThenNumberOfDirectoriesCreatedIs(int expectedNumberOfCreatedDirectories)
        {
            int numberOfDirectories = this.rootDir.GetNumberOfContainedDirectories();
            Assert.AreEqual(this.numbersOfDirectoriesBeforeTest + expectedNumberOfCreatedDirectories, numberOfDirectories);
        }

        protected void ThenNumberOfFilesCreatedIs(int expectedNumberOfCreatedFiles)
        {
            int numberOfFiles = this.rootDir.GetNumberOfContainedFiles();
            Assert.AreEqual(this.numbersOfFilesBeforeTest + expectedNumberOfCreatedFiles, numberOfFiles);
        }

    }
}