﻿using System;
using DosBox.Command.Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DosBoxTest.Command.Library
{
    [TestClass]
    public class CmdTypeTest : CmdTest
    {
        [TestInitialize]
        public void setUp()
        {
            // Check this filestructure in base class: crucial to understand the tests.
            //this.CreateTestFileStructure();
            this.CreateTestFileStructure();

            // Add all commands which are necessary to execute this unit test
            // Important: Other commands are not available unless added here.
            commandInvoker.AddCommand(new CmdType("type", drive));


        }
        [TestMethod]
        public void CmdType_NoArgumentsPassed_ErrorPrinted()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("type");
            ThenOutputContains("syntax of the command is incorrect");
        }

        [TestMethod]
        public void CmdType_MissedFileName_ErrorPrinted()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("type a");
            ThenOutputContains("The system cannot find the file specified.");
        }



        [TestMethod]
        public void CmdType_MoreThenOneArgumentPassed_ErrorPrinted()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("type file1 file2");
            ThenOutputContains("syntax of the command is incorrect");
        }

        [TestMethod]
        public void CmdType_ReadFile_FileContentPrinted()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("type FileInRoot1");
            ThenOutputContains("an entry");
        }

    }
}
