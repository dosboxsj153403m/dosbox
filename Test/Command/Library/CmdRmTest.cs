﻿using System;
using DosBox.Command.Library;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DosBoxTest.Command.Library
{
    [TestClass]
    public class CmdRmTest : CmdTest
    {
        [TestInitialize]
        public void setUp()
        {
            
            this.CreateTestFileStructure();
            commandInvoker.AddCommand(new CmdRm("rm", drive));

        }

        [TestMethod]
        public void CmdRm_RemoveDirectory_RemovedDirectory()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("rm subDir2");
            ThenNumberOfDirectoriesCreatedIs(-1);

        }

        [TestMethod]
        public void CmdRm_RemoveFile_RemovedFile()
        {
            GivenStandardDirectoryStructure();
            AndCurrentDirectoryIsRoot();
            WhenExecutingCommand("rm FileInRoot1");
            ThenNumberOfFilesCreatedIs(-1);
        }

        [TestMethod]
        public void CmdRm_NoParametersPassed_ErrorPrinted()
        {
            GivenStandardDirectoryStructure();
            WhenExecutingCommand("rm");
            ThenOutputContains("syntax of the command is incorrect");
        }

        [TestMethod]
        public void CmdRm_MoreParametersPassed_ErrorPrinted()
        {
            GivenStandardDirectoryStructure();
            WhenExecutingCommand("rm dir1 dir2");
            ThenOutputContains("syntax of the command is incorrect");
        }

        [TestMethod]
        public void CmdRm_WrongFileOrDirectoryNamePassed_ErrorPrinted()
        {
            GivenStandardDirectoryStructure();
            WhenExecutingCommand("rm dir1");
            ThenOutputContains("The system cannot find the file or directory specified.");
        }
    }
}
